//
//  File.swift
//  ais_client
//
//  Created by Дмитрий Брагин on 1/7/18.
//  Copyright © 2018 Дмитрий Брагин. All rights reserved.
//

import Foundation

class ApiService
{
    static let apiPath = "http://localhost:5000/"
        
    static func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    
    static func getQueryString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return "?" + data.map { String($0) }.joined(separator: "&")
    }
    
    static func callPost(on route: String, params:[String:Any], finish: @escaping ((message:String, data:Data?)) -> Void)
    {
        let url = URL(string: apiPath + route)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        let postString = self.getPostString(params: params)
        request.httpBody = postString.data(using: .utf8)
        
        var result:(message:String, data:Data?) = (message: "Fail", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            if(error != nil)
            {
                result.message = "Fail Error not null : \(error.debugDescription)"
            }
            else
            {
                result.message = "Success"
                result.data = data
            }
            
            finish(result)
        }
        task.resume()
    }
    
    static func callGet(on route: String, finish: @escaping ((message: String, data: Data?)) -> Void ) {
        let url = URL(string: apiPath + route)!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        var result:(message:String, data:Data?) = (message: "Fail", data: nil)
        let task = URLSession.shared.dataTask(with: request) {data, response, error in
            if let err = error {
                result.message = "Fail Error not null : \(err.localizedDescription)"
            } else {
                result.message = "Success"
                result.data = data
            }
            
            finish(result)
        }
        task.resume()
    }
    
    static func callPut(on route: String, params:[String:Any], finish: @escaping ((message: String, data: Data?)) -> Void ) {
        let url = URL(string: apiPath + route)!
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        
        let postString = self.getPostString(params: params)
        request.httpBody = postString.data(using: .utf8)
        
        var result:(message:String, data:Data?) = (message: "Fail", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            if(error != nil)
            {
                result.message = "Fail Error not null : \(error.debugDescription)"
            }
            else
            {
                result.message = "Success"
                result.data = data
            }
            
            finish(result)
        }
        task.resume()
    }
    
    static func callDelete(on route: String, queryParams:[String:Any], finish: @escaping ((message: String, data: Data?)) -> Void ) {
        let url = URL(string: apiPath + route + getQueryString(params: queryParams))!
        print(url.absoluteString)
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        
        let postString = self.getPostString(params: queryParams)
        request.httpBody = postString.data(using: .utf8)
    
        
        var result:(message:String, data:Data?) = (message: "Fail", data: nil)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            
            if(error != nil)
            {
                result.message = "Fail Error not null : \(error.debugDescription)"
            }
            else
            {
                result.message = "Success"
                result.data = data
            }
            
            finish(result)
        }
        task.resume()
    }
}
