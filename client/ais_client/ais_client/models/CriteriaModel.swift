//
//  CriteriaModel.swift
//  ais_client
//
//  Created by Дмитрий Брагин on 1/8/18.
//  Copyright © 2018 Дмитрий Брагин. All rights reserved.
//

import Foundation

class CriteriaModel {
    var maxRate: Double = 0
    var minRate: Double = 0
    var maxScore: Double = 0
    var minScore: Double = 0
    
    private var view: CriteriaViewControler
    
    init(view: CriteriaViewControler ) {
        self.view = view
    }
    
    func upload() {
        ApiService.callGet(on: "criteria/", finish: onFinishUpload)
    }
    
    func onFinishUpload(message:String, data:Data?) {
        do {
            if let jsonData = data {
                let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! [String: Any]
                self.maxRate = parsedData["max_rate"] as! Double
                self.minRate = parsedData["min_rate"] as! Double
                self.maxScore = parsedData["max_score"] as! Double
                self.minScore = parsedData["min_score"] as! Double
                DispatchQueue.main.async{
                    self.view.updateView()
                }
            }
        } catch {
            print(error)
        }
    }
    
    func update(maxRate: Double, minRate: Double, maxScore: Double, minScore: Double) {
        let params = ["max_rate": maxRate, "min_rate": minRate, "max_score": maxScore, "min_score": minScore]
        print(params)
        ApiService.callPost(on: "criteria/", params: params, finish: onFinishUpload)
    }
}
