//
//  StudentsListModel.swift
//  ais_client
//
//  Created by Дмитрий Брагин on 1/7/18.
//  Copyright © 2018 Дмитрий Брагин. All rights reserved.
//

import Foundation

class StudentListModel {
    enum Category {
        case elimination
        case payment
        case other
    }
    
    private var students = [String]()
    private let category: Category
    private var view: StudentListViewContoler
    
    init(_ category: Category, to view: StudentListViewContoler) {
        self.category = category
        self.view = view
    }
    
    func uploadList() {
        print("Start upload students list for \(category)")
        switch category {
        case .elimination:
            ApiService.callGet(on: "student/elimination/", finish: finishUploadList)
        case .payment:
            ApiService.callGet(on: "student/payment/", finish: finishUploadList)
        default:
            return
        }
    }
    
    func regenerateList() {
        print("Start upload students list for \(category)")
        switch category {
        case .elimination:
            ApiService.callPut(on: "student/elimination/", params: [:], finish: finishUploadList)
        case .payment:
            ApiService.callPut(on: "student/payment/", params: [:], finish: finishUploadList)
        default:
            return
        }
    }
    
    func finishUploadList(message:String, data:Data?) -> Void
    {
        do {
            if let jsonData = data {
                let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! [String: Any]
                if let students = parsedData["students"] as? [String] {
                    self.students = students
                    print("Update for \(self.category)  finished; students: \(self.students)")
                    DispatchQueue.main.async{
                        self.view.tableView.reloadData()
                    }
                }
            }
        } catch {
            print(error)
        }
    }
    
    var count: Int {
        get {
            return students.count
        }
    }
    
    func get(at index: Int) -> String {
        return students[index]
    }
    
    func remove(at index: Int) {
        print("Start remove students list for \(category)")
        let params = ["name": self.students[index]]
        switch category {
        case .elimination:
            ApiService.callDelete(on: "student/elimination/", queryParams: params , finish: finishUploadList)
        case .payment:
            ApiService.callDelete(on: "student/payment/", queryParams: params, finish: finishUploadList)
        default:
            return
        }
        self.students.remove(at: index)
    }
    
    func finishRemove(message:String, data:Data?) -> Void
    {
        if message == "Success" {
            DispatchQueue.main.async{
                self.view.tableView.reloadData()
            }
        }
    }
    
}
