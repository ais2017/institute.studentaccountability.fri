//
//  ViewController.swift
//  ais_client
//
//  Created by Дмитрий Брагин on 1/6/18.
//  Copyright © 2018 Дмитрий Брагин. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    

    @IBAction func onLoginButtonClicked(_ sender: UIButton) {
        errorLabel.isHidden = true
        if let login = loginField.text {
            if let password = passwordField.text {
                print("\(login): \(password)")
                let params = ["login":login, "password":password]
                ApiService.callPost(on: "auth/" , params: params, finish: finishLogin)
            }
        }
    }
    
    func finishLogin(message:String, data:Data?) -> Void
    {
        DispatchQueue.main.async {
            do
            {
                if let jsonData = data
                {
                    let parsedData = try JSONSerialization.jsonObject(with: jsonData) as! [String: Any]
                    print(parsedData)
                    if let token = parsedData["token"] as? String {
                        print(token)
                        self.performSegue(withIdentifier: "homeScreen", sender: nil)
                    } else {
                        self.errorLabel.isHidden = false
                    }
                }
            }
            catch
            {
                self.errorLabel.isHidden = false
                self.errorLabel.text = "Parse Error: \(error)"
            }
        }
    }
}

