//
//  StudentsListViewControler.swift
//  ais_client
//
//  Created by Дмитрий Брагин on 1/7/18.
//  Copyright © 2018 Дмитрий Брагин. All rights reserved.
//

import UIKit

class StudentListViewContoler : UITableViewController {
    // MARK: - UITableViewDataSource
    var students: StudentListModel? = nil
    
    var category: StudentListModel.Category {
        get {
            return StudentListModel.Category.other
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        students = StudentListModel(self.category, to: self)
        students!.uploadList()
    }
        
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentCell", for: indexPath)
        cell.textLabel?.text = students!.get(at: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            students!.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }
}

class PaymentListViewContoler : StudentListViewContoler {
    // MARK: - UITableViewDataSource
    override var category: StudentListModel.Category {
        get {
            return StudentListModel.Category.payment
        }
    }
    
    @IBAction func regenerateList(_ sender: Any) {
        students!.regenerateList()
    }
}

class EliminationListViewContoler : StudentListViewContoler {
    // MARK: - UITableViewDataSource
    override var category: StudentListModel.Category {
        get {
            return StudentListModel.Category.elimination
        }
    }
    
    @IBAction func regenerateList(_ sender: Any) {
        students!.regenerateList()
    }
}
