//
//  CriteriaViewControler.swift
//  ais_client
//
//  Created by Дмитрий Брагин on 1/8/18.
//  Copyright © 2018 Дмитрий Брагин. All rights reserved.
//

import UIKit

class CriteriaViewControler : UIViewController {
    
    @IBOutlet weak var maxRateField: UITextField!
    @IBOutlet weak var minRateField: UITextField!
    @IBOutlet weak var maxScoreField: UITextField!
    @IBOutlet weak var minScoreField: UITextField!
    
    var criteria: CriteriaModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.criteria = CriteriaModel(view: self)
        self.criteria.upload()
    }
    
    @IBAction func onButtonSaveClicked(_ sender: Any) {
        self.criteria.update(maxRate: Double(maxRateField.text!) ?? 0, minRate: Double(minRateField.text!) ?? 0, maxScore: Double(maxScoreField.text!) ?? 0, minScore: Double(minScoreField.text!) ?? 0)
    }
    
    @IBAction func onButtonResetToDefaultClicked(_ sender: Any) {
        self.updateView()
    }
    
    func updateView() {
        maxRateField.text = String(self.criteria!.maxRate)
        minRateField.text = String(self.criteria!.minRate)
        maxScoreField.text = String(self.criteria!.maxScore)
        minScoreField.text = String(self.criteria!.minScore)
    }
    
}
