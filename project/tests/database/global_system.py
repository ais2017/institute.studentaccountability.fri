import unittest
from src.database.global_system import students


class TestStudents(unittest.TestCase):
    students_count = 20

    def test_count(self):
        self.assertEqual(len(students), self.students_count)

    def test_last_student(self):
        print(students[self.students_count-1].to_representation())


