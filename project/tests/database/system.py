import unittest
from src.controls.system import DaoCriteria, DaoStudentList, Criteria
from src.database.database import clear_criteria, clear_stutents_list


class TestCriteria(unittest.TestCase):
    @clear_criteria
    def test_create(self):
        self.assertTrue(DaoCriteria.create(Criteria(1, 1, 1, 1)))
        self.assertFalse(DaoCriteria.create(Criteria(1, 1, 1, 1)))

    @clear_criteria
    def test_update(self):
        self.assertTrue(DaoCriteria.update(Criteria(1, 1, 1, 1)))

    @clear_criteria
    def test_get(self):
        self.assertTrue(DaoCriteria.create(Criteria(1, 1, 1, 1)))
        criteria = DaoCriteria.get()
        self.assertEqual(criteria.max_rate, 1)
        self.assertEqual(criteria.max_score, 1)
        self.assertEqual(criteria.min_score, 1)
        self.assertEqual(criteria.min_rate, 1)


class TestPaymentList(unittest.TestCase):
    @clear_stutents_list
    def test_create(self):
        self.assertTrue(DaoStudentList.create("Test", DaoStudentList.action_payment))
        self.assertFalse(DaoStudentList.create("Test", DaoStudentList.action_elimination))

    @clear_stutents_list
    def test_update(self):
        self.assertTrue(DaoStudentList.create("Test", DaoStudentList.action_payment))
        self.assertTrue(DaoStudentList.update('Test', DaoStudentList.action_elimination))

    @clear_stutents_list
    def test_delete(self):
        self.assertTrue(DaoStudentList.create("Test", DaoStudentList.action_payment))
        DaoStudentList.remove_by_name("Test")
        self.assertTrue(DaoStudentList.create("Test", DaoStudentList.action_payment))
