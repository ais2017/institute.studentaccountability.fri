import unittest
from src.controls import users
from src.database.database import clear_users


class TestUsers(unittest.TestCase):
    @clear_users
    def test_create(self):
        self.assertTrue(users.create_user("Admin", "1", True))
        self.assertFalse(users.create_user("Admin", "1", True))

    @clear_users
    def test_update(self):
        self.assertTrue(users.create_user("Admin", "1", True))
        self.assertTrue(users.update_user("Admin", "2", True))
        self.assertTrue(users.update_user("Admin1", "3", True))

    @clear_users
    def test_delete(self):
        self.assertTrue(users.create_user("Admin", "1", True))
        self.assertTrue(users.delete_user("Admin1"))
        self.assertTrue(users.delete_user("Admin"))

    @clear_users
    def test_auth(self):
        self.assertTrue(users.create_user("Admin", "1", True))
        self.assertTrue(users.auth("Admin", "1")[1])
        self.assertTrue(users.auth("Admin", "2")[0] is None)
        self.assertTrue(users.auth("Admin1", "1")[0] is None)
