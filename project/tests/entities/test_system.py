from unittest import TestCase

from src.entities.student import Student
from src.entities.system import EliminationList, PaymentList, Criteria, Order


class TestEliminationList(TestCase):
    def test_addition_student(self):
        student = Student("Name", 1)
        l = EliminationList()
        l.add_student(student)
        l.add_student(Student("Name", 1))
        self.assertEqual(len(l.students), 1)
        l.add_student(Student("Name1", 1))
        self.assertEqual(len(l.students), 2)

    def test_removing(self):
        s = Student("Name", 1)
        l = EliminationList()
        l.add_student(s)
        l.add_student(Student("Name1", 1))
        l.add_student(Student("Name3", 1))
        self.assertEqual(len(l.students), 3)
        l.remove_student(s)
        self.assertEqual(len(l.students), 2)

    def test_generation(self):
        students = []
        for i in range(0, 10):
            student = Student("Name{}".format(i), 1)
            student.set_performance(1, 2, i, 10-i, 1)



class TestPaymentList(TestCase):
    def test_addition_student(self):
        s = Student("Name", 1)
        l = PaymentList()
        l.add_student("Name")
        l.add_student("Name")
        self.assertEqual(len(l.students), 1)
        l.add_student("Name1")
        self.assertEqual(len(l.students), 2)

    def test_removing(self):
        s = Student("Name", 1)
        l = PaymentList()
        l.add_student("Name")
        l.add_student("Name1")
        l.add_student("Name2")
        self.assertEqual(len(l.students), 3)
        l.remove_student("Name")
        self.assertEqual(len(l.students), 2)


class TestOrder(TestCase):
    def test_order_creation(self):
        student = Student("Name", 1)
        l = EliminationList()
        l.add_student("Name")
        l.add_student("Name1")

        order = Order.generate_order(l)
        self.assertEqual(order.order_number, 1)
        order = Order.generate_order(l)
        self.assertEqual(order.order_number, 2)
        order = Order.generate_order([student])
        self.assertEqual(order.order_number, 3)




