import unittest
from src.entities.student import Character, Performance, Student


class TestStudentPerformance(unittest.TestCase):
    def test_average_score(self):
        p = Performance(1, 0, 0, 2, 0)
        self.assertEqual(p.average_score, 4)

        p = Performance(1, 0, 0, 2, 2)
        self.assertEqual(p.average_score, 4.5)


class TestSudentRepresentation(unittest.TestCase):
    def test_achievement_repr(self):
        print(Character("Name", 1, "Desc").to_representation())

    def test_performance_repr(self):
        print(Performance(1, 1, 2, 3, 4).to_representation())

    def test_student_repr(self):
        student = Student("Name", 1)
        print(student.to_representation())

        student.add_character("Name", 1, "Desc")
        print(student.to_representation())

        student.set_performance(1, 1, 2, 3, 4)
        print(student.to_representation())
