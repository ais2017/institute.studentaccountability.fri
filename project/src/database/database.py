import sqlalchemy
from sqlalchemy import create_engine, MetaData
from sqlalchemy import Table, Column, Integer, String, ForeignKey, Boolean, Float


engine = create_engine('postgresql://postgres:12345@localhost/ais', client_encoding='utf8')
meta = MetaData(bind=engine, reflect=True)

Users = Table('users', meta,
              Column('name', String, primary_key=True),
              Column('passwd', String),
              Column('token', String, default=''),
              Column('is_admin', Boolean, default=True),
              extend_existing=True
              )

Criteria = Table('criteria', meta,
                 Column('id', Integer, primary_key=True),
                 Column('min_score', Float, nullable=False),
                 Column('max_score', Float, nullable=False),
                 Column('min_rate', Float, nullable=False),
                 Column('max_rate', Float, nullable=False),
                 extend_existing=True
                 )

StudentsList = Table('students_list', meta,
                     Column('name', String, primary_key=True),
                     Column('action', String(1)),
                     extend_existing=True
                     )

# Create the above tables
meta.create_all(engine, checkfirst=True)


def clear_users(f):
    def wrapper(self):
        engine.execute(Users.delete())
        f(self)
        engine.execute(Users.delete())

    return wrapper


def clear_criteria(f):
    def wrapper(self):
        engine.execute(Criteria.delete())
        f(self)
        engine.execute(Criteria.delete())

    return wrapper


def clear_stutents_list(f):
    def wrapper(self):
        engine.execute(StudentsList.delete())
        f(self)
        engine.execute(StudentsList.delete())

    return wrapper


def execure_query(query):
    try:
        engine.execute(query)
        return True
    except sqlalchemy.exc.IntegrityError as e:
        print(e)
        return False
