from src.entities.student import Student, Character, Performance


def create_student(number, positive, good_performance, semester=1):
    some_name = "Name{}".format(number)
    count_2 = 0 if good_performance else 6 if positive else 12
    count_5 = 6 if good_performance else 0
    return Student(name=some_name,
                   semester=semester,
                   characters=[Character(some_name, (1 if positive else -1) * semester)],
                   semester_to_performance={s: Performance(s, count_2, 4, 5, count_5) for s in range(1, semester + 1)}
                   )


# TODO add some students


students = list()

for i in range(20):
    students.append(create_student(i, bool(i % 2), bool(i % 3), i % 4 + 1))

students.append(Student(name="BetterStudent",
                        semester=2,
                        characters=[Character("TEst", 10)],
                        semester_to_performance={s: Performance(s, 0, 0, 0, 21) for s in range(1, 3)}
                        ))
