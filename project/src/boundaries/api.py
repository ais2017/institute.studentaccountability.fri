import json

from flask import Flask, request

from src.controls.system import DaoStudentList, DaoCriteria, EliminationList, PaymentList, Criteria
from src.database.global_system import students


app = Flask(__name__)
DaoCriteria.create(Criteria(2, 4, 0, 1))


@app.route('/auth/', methods=['POST'])
def auth():
    """
    Request for user auth.
    :param login: user name
    :param password: user password
    :return: token or None if wrong auth credentionals
    """
    from src.controls.users import auth
    # app.logger.debug('Request form', request.form)
    print('Request form ', request.form)
    login = request.form.get('login', None)
    password = request.form.get('password', None)

    token, is_admin = auth(login, password)
    response = {"is_admin": is_admin, "token": token}
    print("Auth response ", response)
    return json.dumps(response), 200 if token else 400


@app.route('/users/', methods=['POST'])
def create_user():
    from src.controls.users import create_user
    login = request.form.get("login")
    passwd = request.form.get("passwd")
    is_admin = request.form.get("is_admin")
    return "", 200 if create_user(login, passwd, is_admin) else 400


@app.route('/student/payment/', methods=['GET', 'PUT', 'DELETE'])
def get_payment_list():
    if request.method == 'GET':
        return get_student_list(PaymentList, DaoStudentList.action_payment)
    elif request.method == 'PUT':
        return regenerate_students_list(PaymentList, DaoStudentList.action_payment).to_representation()
    elif request.method == 'DELETE':
        name = request.form.get("name")
        DaoStudentList.remove_by_name(name)
        return "", 204
    return "", 405


@app.route('/student/elimination/', methods=['GET', 'PUT', 'DELETE'])
def get_elimination_list():
    if request.method == 'GET':
        return get_student_list(EliminationList, DaoStudentList.action_elimination)
    elif request.method == 'PUT':
        return regenerate_students_list(EliminationList, DaoStudentList.action_elimination).to_representation()
    elif request.method == 'DELETE':
        print(request.args)
        name = request.args.get("name")
        print(name)
        DaoStudentList.remove_by_name(name)
        return "", 204
    return "", 405


def get_student_list(list_cls, action):
    students_list = DaoStudentList.get(action)
    if students_list is None or len(students_list.students) == 0:
        students_list = list_cls()
    return students_list.to_representation()


def regenerate_students_list(list_cls, action):
    print("Generate list for ", list_cls.__name__)
    DaoStudentList.remove_by_action(action)
    students_list = list_cls()
    students_list.generate_list(students, DaoCriteria.get())
    for student in students_list.students:
        DaoStudentList.create(student, action)
    return students_list


@app.route('/criteria/', methods=['GET', 'POST'])
def criteria():
    if request.method == 'GET':
        res = DaoCriteria.get().to_representation()
        print(res)
        return res
    elif request.method == 'POST':
        print(request.form)
        max_rate = request.form.get('max_rate')
        min_rate = request.form.get('min_rate')
        max_score = request.form.get('max_score')
        min_score = request.form.get('min_score')
        _criteria = Criteria(float(min_score), float(max_score), float(max_rate), float(min_rate))
        DaoCriteria.update(_criteria)
        res = DaoCriteria.get().to_representation()
        print (res)
        return res
    return "", 405


def dismiss_student(user, data):
    """
    Request for dismissing student
    :param user: user made the request
    :param data: json contains field student_name - student on dismissal
    :return:
    """
    pass



