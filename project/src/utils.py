import json


class Serializable:
    def to_representation(self):
        def serialize(obj):
            """JSON serializer for objects not serializable by default json code"""
            if isinstance(obj, Serializable):
                return obj.to_representation()
            return obj.__dict__

        return json.dumps(self.__dict__, default=serialize)
