from src.entities.student import Student
from src.utils import Serializable


class Criteria(Serializable):
    def __init__(self, min_score, max_score, max_rate, min_rate):
        self.min_rate = min_rate
        self.max_rate = max_rate
        self.max_score = max_score
        self.min_score = min_score


class StudentsList(Serializable):
    def __init__(self):
        self.students = list()

    def remove_student(self, student: str):
        self.students.remove(student)

    def add_student(self, student: str):
        self.students.append(student)

    def generate_list(self, students: list, criteria: Criteria):
        raise NotImplementedError()

    @staticmethod
    def _get_student_rate(student: Student) -> int:
        rate = 0
        for character in student.characters:
            rate += character.rate
        return rate


class EliminationList(StudentsList):
    def generate_list(self, students: list, criteria: Criteria):
        for student in students:
            rate = self._get_student_rate(student)
            if criteria.max_rate > rate:
                if criteria.max_score > student.semester_to_performance[student.semester].average_score:
                    self.add_student(student.name)


class PaymentList(StudentsList):
    def generate_list(self, students: list, criteria: Criteria):
        for student in students:
            # print(student.name, self._get_student_rate(student), student.semester_to_performance[student.semester].average_score)
            rate = self._get_student_rate(student)
            if criteria.min_rate < rate:
                if criteria.min_score < student.semester_to_performance[student.semester].average_score:
                    self.add_student(student.name)


class Order(Serializable):
    order_id = 0

    def __init__(self, students_name):
        self.order_number = Order.order_id
        self.students_name = students_name

    @classmethod
    def generate_order(cls,  elimination_list):
        cls.order_id += 1
        if elimination_list is EliminationList:
            return Order([student.name for student in elimination_list.students])
        if elimination_list is list:
            return Order([student.name for student in elimination_list])
        return Order(list())
