from src.utils import Serializable


class Character(Serializable):
    def __init__(self, achievement_type="", rate=0, description=""):
        self.achievement_type = achievement_type
        self.rate = rate
        self.description = description


class Performance(Serializable):
    def __init__(self, semester, *args):
        self.semester = semester
        if len(args) != 4:
            raise ValueError("Performance has 5 arguments")
        self.score_to_count = {i+2: args[i] for i in range(0, 4)}

    @property
    def average_score(self) -> float:
        s, c = 0, 0
        for score, count in self.score_to_count.items():
            s += score * count
            c += count
        return s/c


class Student(Serializable):
    def __init__(self, name: str, semester: int, characters=list(), semester_to_performance=dict()):
        self.semester = semester
        self.name = name
        self.characters = characters
        self.semester_to_performance = semester_to_performance

    def add_character(self, achievement_type: str, rate: int, description: str) -> None:
        self.characters.append(Character(achievement_type, rate, description))

    def set_performance(self, semester, *args):
        self.semester_to_performance[semester] = Performance(semester, *args)

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name and self.semester == other.semester
