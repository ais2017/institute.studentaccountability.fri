from src.entities.student import Student
from src.database.global_system import students


class DaoStudent:
    @staticmethod
    def get_students() -> list:
        """
        Request for getting students from db
        :return: returns list of students
        """

        return students

    @staticmethod
    def add_student(student: Student):
        """
        Request for append student into db
        :param student: information about the student which you want to add
        :return:
        """
        students.append(student)
        return

    @staticmethod
    def update_student(student: Student):
        """
        Request for updating student info into db
        :param student: information about the student which you want to update
        :return:
        """

        return

    @staticmethod
    def remove_student(student: Student):
        """
        Request for removing student from db
        :param student: information about the student which you want to remove
        :return:
        """
        return
