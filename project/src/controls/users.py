import random
import string

import sqlalchemy

from src.database.database import engine, Users


def auth(name, passwd):
    print("Auth params:", name, passwd)
    auth_user = Users.select().where(Users.c.name == name).where(Users.c.passwd == passwd)
    res = engine.execute(auth_user)
    user = res.fetchone()
    print("Auth: ", user)
    if user is not None:
        return user['token'], user['is_admin']
    return None, None


def create_user(name, passwd, is_admin=False):
    try:
        token = ''.join(random.choices(string.ascii_uppercase + string.digits, k=32))
        user = Users.insert().values(name=name, passwd=passwd, is_admin=is_admin, token=token)
        engine.execute(user)
        return True
    except sqlalchemy.exc.IntegrityError as e:
        print(e)
        return False


def update_user(name, passwd, is_admin=False):
    try:
        user = Users.update().where(Users.c.name == name).values(passwd=passwd, is_admin=is_admin)
        engine.execute(user)
        return True
    except sqlalchemy.exc.IntegrityError as e:
        print(e)
        return False


def delete_user(name):
    try:
        user = Users.delete().where(Users.c.name == name)
        engine.execute(user)
        return True
    except sqlalchemy.exc.IntegrityError as e:
        print(e)
        return False

