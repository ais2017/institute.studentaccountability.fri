from src.entities.system import Criteria, EliminationList, PaymentList
from src.database import database


class DaoCriteria:
    @staticmethod
    def create(criteria: Criteria):
        query = database.Criteria.insert().values(
            max_rate=criteria.max_rate,
            min_rate=criteria.min_rate,
            min_score=criteria.min_score,
            max_score=criteria.max_score,
            id=1
        )
        return database.execure_query(query)

    @staticmethod
    def update(criteria: Criteria):
        query = database.Criteria.update(whereclause=(database.Criteria.c.id == 1)).values(
            max_rate=criteria.max_rate,
            min_rate=criteria.min_rate,
            min_score=criteria.min_score,
            max_score=criteria.max_score
        )
        return database.execure_query(query)

    @staticmethod
    def get() -> Criteria:
        query = database.Criteria.select().where(database.Criteria.c.id == 1)
        res = database.engine.execute(query)
        criteria = res.fetchone()
        if criteria is not None:
            return Criteria(
                criteria['min_score'],
                criteria['max_score'],
                criteria['max_rate'],
                criteria['min_rate']
            )
        return Criteria(1, 2, -1, 0)


class DaoStudentList:
    action_payment = 'p'
    action_elimination = 'e'

    @staticmethod
    def create(name, action):
        query = database.StudentsList.insert().values(
            name=name, action=action
        )
        return database.execure_query(query)

    @staticmethod
    def update(name, action):
        query = database.StudentsList.update(whereclause=(database.StudentsList.c.name == name)).values(
            action=action
        )
        return database.execure_query(query)

    @staticmethod
    def remove_by_name(name):
        query = database.StudentsList.delete(whereclause=(database.StudentsList.c.name == name))
        return database.execure_query(query)

    @staticmethod
    def remove_by_action(action):
        query = database.StudentsList.delete(whereclause=(database.StudentsList.c.action == action))
        return database.execure_query(query)

    @staticmethod
    def get(action):
        query = database.StudentsList.select(whereclause=(database.StudentsList.c.action == action))
        res = database.engine.execute(query)
        l = res.fetchall()
        if l is None:
            return None

        if action == DaoStudentList.action_elimination:
            result = EliminationList()
        elif action == DaoStudentList.action_payment:
            result = PaymentList()
        else:
            return None

        if isinstance(l, list):
            for item in l:
                result.add_student(item['name'])
        else:
            result.add_student(l['name'])

        return result
